#include <SFML/Graphics.hpp>
#include "fonction.hpp"

//Ecrit un text
void ecritText(char textAEcrire[], Font police,Color couleur, int taille, int posX, int posY, RenderWindow &app)
{
    //initialisation du text
    Text text(textAEcrire, police, taille);
    //met la couleur demand� au texte
    text.setFillColor(couleur);
    //on lui donne une position
    text.setPosition(posX, posY);
    //on dessinne le text
    app.draw(text);
}
int menu(RenderWindow &app, Font MyFont)
{
    int jouer = 0;
    // le choix sert a deffinir la position dans le menu
    int choix =1;
    //menu est la condition de la boulcle, elle passe a zero quand on appui sur entrer
    int continueMenu = 1;
    //initialisation du text a afficher
    char nomJeu[] = "Mortal Resurrection", play[] = "Play", synopsys[]= "Synopsis", regles[]= "R�gles";
    //initialisation des couleurs la couleurRSelect est plus opaque : opacit� = 255 au lieu de 150 pour l'autre
    Color couleurR(103,244,177,150), couleurTitre(244, 249,178), couleurRSelect(103,244,177,255);

    Texture textureBackground;
    if (!textureBackground.loadFromFile("Ressources/fondMenu.jpg"))
        printf("erreur lors du chargement du background");
    textureBackground.setSmooth(true);
    Sprite spriteBackground(textureBackground);
    spriteBackground.scale(0.3f, 0.3f);


    while(continueMenu)
    {

        // Clear screen
        app.clear();

        // Fonction qui gere les evennement : fleche du bas, fleche du heut et entr�
        gestionEventMenu(app, choix, continueMenu, jouer);
        app.draw(spriteBackground);
        // Ecrit le nom du jeu qu'on a du entrer au dessus
        ecritText(nomJeu, MyFont, couleurTitre, 110, 10, -20, app);

        // on remet choix entre 1 et 3 vu qu'on a que 3 oprion dans le menu
        if (choix > 3)
            choix = 1;
        else if(choix < 1)
            choix = 3;

        // switch qui sert a avoir une opacit� differente suivant le choix : couleurSelect est plus opaque
        switch(choix)
        {

        case 1:
            ecritText(play, MyFont, couleurRSelect, 95, 230,250, app);
            ecritText(synopsys, MyFont, couleurR, 95, 230,350, app);
            ecritText(regles, MyFont, couleurR, 95, 230,450, app);
            break;
        case 2:
            ecritText(play, MyFont, couleurR, 95, 230,250, app);
            ecritText(synopsys, MyFont, couleurRSelect, 95, 230,350, app);
            ecritText(regles, MyFont, couleurR, 95, 230,450, app);
            break;
        case 3:
            ecritText(play, MyFont, couleurR, 95, 230,250, app);
            ecritText(synopsys, MyFont, couleurR, 95, 230,350, app);
            ecritText(regles, MyFont, couleurRSelect, 95, 230,450, app);
            break;

        }
        if (jouer != 4)
            jouer = choix;
        // Update the window
        app.display();
    }
    return jouer;
}

void gestionEventMenu(RenderWindow &app, int &choix, int &continuBoucle, int &play)
{

    // Process events
    Event event;
    while(app.pollEvent(event))
    {
        switch (event.type)
        {
        // fen�tre ferm�e
        case Event::Closed:
            app.close();
            continuBoucle = 0;
            play = 4;
            break;

        // touche press�e
        case Event::KeyPressed:
            if (event.key.code == Keyboard::Down)
                choix += 1;
            if (event.key.code == Keyboard::Up)
                choix -= 1;

            if (event.key.code == Keyboard::Enter)
                continuBoucle = 0;
            break;


        // we don't process other types of events
        default:
            break;
        }
    }
}

void regles(RenderWindow &app, Font &MyFont)
{
    int continuBoucle = 1;
    Texture textureBackground;
    if (!textureBackground.loadFromFile("Ressources/fondMenu.jpg"))
        printf("erreur lors du chargement du background");
    textureBackground.setSmooth(true);
    Sprite spriteBackground(textureBackground);
    spriteBackground.scale(0.3f, 0.3f);
    Color couleurR(255,255,255);
    Color couleurB(255,255,255);

    app.clear();
    app.draw(spriteBackground);

    char regle1[] = "Vous devez prot�ger votre slime, s'il meurt, c'est perdu";
    char regle2[] = "Les portes sont bloqu�s par des barreaux, trouver un autre chemin !";
    char regle3[] = "Vous ne pouvez plus retourner dans une salle compl�t�e";
    char regle4[] = "Vous avez 3 points de vie, s'il tombent � 0 c'est perdu";
    char regle5[] = "Les ennemis sont faibles, ils meurent en 1 coups, mais ils sont nombreux !";
    char regle6[] = "Les ennemis sont r�sistants, viser les jambes ne leur fera rien !";
    char regle7[] = "Controles :";
    char regle8[] = "Utilisez le fl�ches de votre clavier pour vous deplacer et espace pour attaquer !";

    ecritText(regle1, MyFont, couleurR, 45, 10,20, app);
    ecritText(regle2, MyFont, couleurR, 45, 10,120, app);
    ecritText(regle3, MyFont, couleurR, 45, 10,220, app);
    ecritText(regle4, MyFont, couleurR, 45, 10,320, app);
    ecritText(regle5, MyFont, couleurR, 45, 10,420, app);
    ecritText(regle6, MyFont, couleurR, 45, 10,520, app);
    ecritText(regle7, MyFont, couleurR, 90, 10,580, app);
    ecritText(regle8, MyFont, couleurB, 45, 10,680, app);

    app.display();
    Event event;

    while (continuBoucle == 1)
    {
        while(app.pollEvent(event))
        {
            switch (event.type)
            {
            // fen�tre ferm�e
            case Event::Closed:
                app.close();
                continuBoucle = 0;
                break;

            // touche press�e
            case Event::KeyPressed:
                if (event.key.code == Keyboard::Enter)
                    continuBoucle = 0;
                break;


            // we don't process other types of events
            default:
                break;
            }
        }
    }
}

void synopsis (RenderWindow &app, Font &MyFont)
{
    int continuBoucle = 1;

    Texture textureBackground;
    if (!textureBackground.loadFromFile("Ressources/fondMenu.jpg"))
        printf("erreur lors du chargement du background");
    textureBackground.setSmooth(true);
    Sprite spriteBackground(textureBackground);
    spriteBackground.scale(0.3f, 0.3f);
    Color couleurR(0,0,0);
    app.clear();
    app.draw(spriteBackground);
    char syn1[] = "Bienvenue dans mortal RESURECTION, un  rpg fantaisiste o� le but est";
    char syn2[] = "de sortir du donjon.";
    char syn3[] = "Le ninja s'est retrouv� coinc� dans le donjon, d�pouill� de toutes ses";
    char syn4[] = "armes.";
    char syn5[] = "Le ninja s'arme d'une lance pour regagner sa libert�.";
    ecritText(syn1, MyFont, couleurR, 45, 10,200, app);
    ecritText(syn2, MyFont, couleurR, 45, 10,250, app);
    ecritText(syn3, MyFont, couleurR, 45, 10,300, app);
    ecritText(syn4, MyFont, couleurR, 45, 10,350, app);
    ecritText(syn5, MyFont, couleurR, 45, 10,400, app);
    app.display();
    Event event;

    while (continuBoucle == 1)
    {
        while(app.pollEvent(event))
        {
            switch (event.type)
            {
            // fen�tre ferm�e
            case Event::Closed:
                app.close();
                continuBoucle = 0;
                break;

            // touche press�e
            case Event::KeyPressed:
                if (event.key.code == Keyboard::Enter)
                    continuBoucle = 0;
                break;


            // we don't process other types of events
            default:
                break;
            }
        }
    }
}
