#include <SFML/Graphics.hpp>
#include "fonction.hpp"
#include <math.h>


void attaqueJoueurTouche(Entite &player, Entite &mob, Time &dtAttaque, Clock &clock)
{
    dtAttaque = clock.getElapsedTime();
    int attaque = 0;
    if (Keyboard::isKeyPressed(Keyboard::Space))
    {
        if (dtAttaque.asSeconds() > 0.7)
        {
            attaque = 1;
            dtAttaque= clock.restart();
        }
    }
    if (dtAttaque.asSeconds() < 0.3)
    {
        attaque = 1;
    }
    if(attaque)
    {
        if (mob.posY  < player.posY +20 && mob.posY + HAUTEUR_MOB >  player.posY +20)
        {
            if (player.numTexture < 3 && mob.posX  < player.posX +50 && mob.posX >  player.posX)
            {
                mob.pointsdevie -= player.degats;
            }


            if (player.numTexture > 2 && mob.posX  < player.posX && mob.posX + HAUTEUR_MOB >  player.posX - 50)
            {
                mob.pointsdevie -= player.degats;
            }
        }
    }


}
void attaqueDuPlayer(Entite &player, RenderWindow &app, Sprite &lanceDroiteSprite, Sprite &lanceGaucheSprite, Time &dtAttaque, Clock &clock)
{
    dtAttaque = clock.getElapsedTime();
    lanceDroiteSprite.setPosition(-60,0);
    lanceGaucheSprite.setPosition(-60,0);
    int attaque = 0;
    if (Keyboard::isKeyPressed(Keyboard::Space))
    {
        if (dtAttaque.asSeconds() > 0.7)
        {
            attaque = 1;
            dtAttaque= clock.restart();
        }
    }
    if (dtAttaque.asSeconds() < 0.3)
    {
        attaque = 1;
    }
    if(attaque)
    {
        if (player.numTexture > 2)
            lanceGaucheSprite.setPosition(player.posX - 45, player.posY + 30);

        if (player.numTexture < 3)
            lanceDroiteSprite.setPosition(player.posX + 27, player.posY + 30);
    }
}



void gestionEventJeu(RenderWindow &app, int &continuBoucle, Time dt, Entite &player, int numFrame)
{


    // Process events
    Event event;
    while(app.pollEvent(event))
    {
        switch (event.type)
        {
        // fen�tre ferm�e
        case Event::Closed:
            app.close();
            continuBoucle = 0;
            break;

        // touche press�e
        case Event::KeyPressed:

            if (event.key.code == Keyboard::Enter)
                continuBoucle = 0;
            break;


        // we don't process other types of events
        default:
            break;
        }
    }
    if (Keyboard::isKeyPressed(Keyboard::Left)) // Fleche gauche appuyee
    {
        if (player.numTexture < 3)
            player.numTexture = 3;

        if (player.speedX > (-player.speed))
            player.speedX -= ACCELERATION;

        else
            player.speedX = (-player.speed);
    }
    else
    {
        if (player.speedX < -100)
            player.speedX += ACCELERATION;

        else if (player.speedX < 0)
            player.speedX += ACCELERATION;

    }
    if (Keyboard::isKeyPressed(Keyboard::Right)) // Fleche droite appuyee
    {
        if (player.numTexture >= 3)
            player.numTexture = 0;
        if (player.speedX < player.speed)
            player.speedX += ACCELERATION;

        else
            player.speedX = player.speed;
    }
    else
    {
        if (player.speedX > 100)
            player.speedX -= ACCELERATION;
        else if (player.speedX > 0)
            player.speedX -= ACCELERATION;

    }

    if (Keyboard::isKeyPressed(Keyboard::Up)) // Fleche du haut appuyee
    {
        if (player.speedY > (-player.speed))
            player.speedY -= ACCELERATION;

        else
            player.speedY = (-player.speed);
    }
    else
    {
        if (player.speedY < -100)
            player.speedY += ACCELERATION;
        else if (player.speedY < 0)
            player.speedY += ACCELERATION;
    }
    if (Keyboard::isKeyPressed(Keyboard::Down)) // Fleche du bas appuyee
    {
        if (player.speedY < player.speed)
            player.speedY += ACCELERATION;

        else
            player.speedY = player.speed;
    }
    else
    {
        if (player.speedY > 100)
            player.speedY -= ACCELERATION;
        else if (player.speedY > 0)
            player.speedY -= ACCELERATION;

    }
    if (player.speedX < ACCELERATION - 1 && player.speedX > - ACCELERATION +1)
        player.speedX = 0;
    if (player.speedY < ACCELERATION - 1 && player.speedY > - ACCELERATION +1)
        player.speedY = 0;

    if (player.speedX > 0 && numFrame % 12 == 0)
    {
        player.numTexture += 1;
        if (player.numTexture > 2)
            player.numTexture = 0;
    }

    else if(player.speedX < 0 && numFrame % 12 == 0)
    {
        player.numTexture += 1;
        if (player.numTexture > 5)
            player.numTexture = 3;
    }

    if (player.speedX == 0)
    {
        if (player.speedY > 0 || player.speedY < 0)
        {
            if( numFrame % 12 == 0)
            {
                if (player.numTexture > 2)
                {
                    player.numTexture += 1;
                    if (player.numTexture > 5)
                        player.numTexture = 3;
                }
                else
                {
                    player.numTexture += 1;
                    if (player.numTexture > 2)
                        player.numTexture = 0;
                }
            }
        }
        else
        {
            if (player.numTexture > 2)
            {

                player.numTexture = 3;
            }


            if (player.numTexture < 3)
                player.numTexture = 0;
        }


    }

    if (player.posX > LARGEUR_FENETRE - 130 && player.speedX > 0 && (player.posY < 270 || player.posY > 370))
        player.speedX = 0;


    else if (player.posX < 100 && player.speedX < 0 && ( player.posY < 340 || player.posY > 370))
        player.speedX = 0;

    if (player.posY > HAUTEUR_FENETRE - 155 && player.speedY > 0 && (player.posX < 535 || player.posX > 590))

        player.speedY = 0;

    else if (player.posY < 50 && player.speedY < 0 && (player.posX < 535 || player.posX > 590))
        player.speedY = 0;


    if(player.posX > LARGEUR_FENETRE - 60 && player.speedX >= 0 || (player.posY < 25 && player.speedY <= 0))
    {
        player.speedX = 0;
        player.speedY = 0;
    }
    else if ((player.posY > HAUTEUR_FENETRE - 90 && player.speedY >= 0) || player.posX < 50 && player.speedX <= 0)
    {
        player.speedX = 0;
        player.speedY = 0;
    }

    float dSpeed = sqrt(player.speedX*player.speedX + player.speedY*player.speedY);

    if (dSpeed > player.speed)
    {
        player.speedX *= player.speed/dSpeed;
        player.speedY *= player.speed/dSpeed ;
    }
    player.posX += player.speedX *dt.asSeconds();

    player.posY += player.speedY *dt.asSeconds();

    player.sprite.setPosition(player.posX, player.posY);
}
