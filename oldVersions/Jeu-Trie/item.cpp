#include <SFML/Graphics.hpp>
#include "fonction.hpp"

void itemCoeur(Entite &player, Item &coeur, RenderWindow &app)
{
    if(player.posX+30 > coeur.posX && player.posX < coeur.posX + 30 && player.posY+60 > coeur.posY && player.posY < coeur.posY + 42)
    {
        if(player.pointsdevie < player.vieMax)
        {
            player.pointsdevie++;
            coeur.actif = 0;
        }
    }
    app.draw(coeur.sprite);
}
