#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "fonction.hpp"

//a pas oublier dans chaque fichiers
using namespace sf;

int main()
{
    srand(time(NULL));
    // Create the main window
    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Jeu");

    app.setFramerateLimit(60);

    //Chargement de la musique
    Music musique1;
    if (!musique1.openFromFile("Ressources/musique/Dungeon-Cave_loop.ogg"))
        printf(("Erreur chargement de la musique"));
    musique1.setPosition(0, 1, 10);
    musique1.setPitch(2);
    musique1.setVolume(1);
    musique1.setLoop(true);

    Texture gameOverTexture;
    if(!gameOverTexture.loadFromFile("Ressources/map/GameOver.png"))
        printf("erreur charge gameover");
    Sprite gameoverSprite;
    gameoverSprite.setTexture(gameOverTexture);

    //chargement d'une la police d'ecriture
    Font MyFont;
    if (!MyFont.loadFromFile(FONT))
    {
        printf("erreur lors du chargement de la police");
    }


    musique1.play();

    int play = 0;
    // boucle tant que la fenetre est ouverte
    while (app.isOpen())
    {
        switch(play)
        {
        case 0 :
            // Lancement du menu
            play = menu(app, MyFont);
            break;

        case 1:
            // Lancement du jeu
            play = jeu(app, MyFont);
            play = 4;
            break;

        case 2 :
            play = 0;
            break;


        case 3 :
            play = 0;
            break;

        case 4 :
            //ecran game over
            app.clear();
            app.draw(gameoverSprite);
            app.display();
            sleep(seconds(2));
            play = 0;
            break;

        case 5:
            //ecran de fin de jeu
            printf("Bravo");
            play = 0;
            break;

        case 7:
            app.close();
            break;
        }


    }

    return 0;
}
