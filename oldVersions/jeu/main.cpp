#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "fonction.hpp"

//a pas oublier dans chaque fichiers
using namespace sf;

int main()
{
    srand(time(NULL));
    // Create the main window
    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Jeu");

    app.setFramerateLimit(60);

    //Chargement de la musique
    Music musique1;
    if (!musique1.openFromFile("Ressources/musique/Dungeon-Cave_loop.ogg"))
        printf(("Erreur chargement de la musique"));
    musique1.setPosition(0, 1, 10);
    musique1.setPitch(2);
    musique1.setVolume(2);
    musique1.setLoop(true);

    //chargement d'une la police d'ecriture
    Font MyFont;
    if (!MyFont.loadFromFile(FONT))
    {
        printf("erreur lors du chargement de la police");
    }


    musique1.play();
    // boucle tant que la fenetre est ouverte
    while (app.isOpen())
    {
        // Lancement du menu
        int play = menu(app, MyFont);

        if(play)
            // Lancement du jeu
            jeu(app, MyFont);
    }

    return 0;
}
