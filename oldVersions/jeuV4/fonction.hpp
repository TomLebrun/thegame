#ifndef FONCTION_HPP_INCLUDED
#include <SFML/Graphics.hpp>
#define FONCTION_HPP_INCLUDED
#define LARGEUR_FENETRE 1200.0
#define HAUTEUR_FENETRE 800.0
#define HAUTEUR_MOB 50
#define FONT "ressources/police/police2.ttf"
#define ACCELERATION 25.0

//a pas oublier dans chaque fichiers
using namespace sf;
typedef struct
{
    Texture texture[20];
    Sprite sprite;
    float posX, posY, speed = 300, speedX = 0, speedY = 0;
    int degats, vieMax=3, pointsdevie, numTexture;
}Entite;

typedef struct{
	Texture texture;
	Sprite sprite;
	float posX = LARGEUR_FENETRE / 2;
	float posY = HAUTEUR_FENETRE / 2;
	int actif = 1;
}Item;

typedef struct
{
    Texture texture;
    Sprite sprite;
    Item coeur;

    int nbMobTireurs, nbMobCAC, spawnPlayer[2], posInitMobCAC[2][4], posInitMobTireurs[2][4], sortie[2][2];
}Salle;


int alea(int min, int max);

void ecritText(char textAEcrire[], Font police, Color couleur, int taille, int posX, int posY, RenderWindow &app);

void gestionEventMenu(RenderWindow &app, int &choix, int &continuBoucle, int &play);

void gestionEventJeu(RenderWindow &app, int &continuBoucle, Time dt, Entite &player, int numFrame);

int menu(RenderWindow &app, Font MyFont);

int jeu(RenderWindow &app, Font MyFont);

void regles (RenderWindow &app, Font &MyFont);

void synopsis (RenderWindow &app, Font &MyFont);

void deplacementMob(Entite &mob, Entite &player, Time dt, RenderWindow &app);

void attaqueDuMob(Entite &player, Entite &mob, Entite &attaqueMob, Time dt, int numFrame);

void deplacementPet( Entite &player,Entite &attaqueMob, Entite &pet, Time dt, RenderWindow &app);

void collisionMob(Entite &mob, Entite &mob1, Entite &player);

void affichageVie (Entite &player);

void attaqueDuPlayer(Entite &player, RenderWindow &app, Sprite &lanceDroiteSprite, Sprite &lanceGaucheSprite, Time &dtAttaque,Clock &clock);

void attaqueMobCAC(Entite &mobCAC, Entite &nbMobCAC, Entite &player, RenderWindow &app);

#endif // FONCTION_HPP_INCLUDED
