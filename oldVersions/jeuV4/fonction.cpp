#include <SFML/Graphics.hpp>
#include "fonction.hpp"
#include <math.h>
//a pas oublier dans chaque fichiers
using namespace sf;

int alea(int min, int max)
{
    int res;
    res = (rand()%(max-(min-1)) + min);
    return res;
}

//Ecrit un text
void ecritText(char textAEcrire[], Font police,Color couleur, int taille, int posX, int posY, RenderWindow &app)
{
    //initialisation du text
    Text text(textAEcrire, police, taille);
    //met la couleur demand� au texte
    text.setFillColor(couleur);
    //on lui donne une position
    text.setPosition(posX, posY);
    //on dessinne le text
    app.draw(text);
}
int menu(RenderWindow &app, Font MyFont)
{
    int jouer = 0;
    // le choix sert a deffinir la position dans le menu
    int choix =1;
    //menu est la condition de la boulcle, elle passe a zero quand on appui sur entrer
    int continueMenu = 1;
    //initialisation du text a afficher
    char nomJeu[] = "Mortal Resurrection", play[] = "Play", synopsys[]= "Synopsis", regles[]= "R�gles";
    //initialisation des couleurs la couleurRSelect est plus opaque : opacit� = 255 au lieu de 150 pour l'autre
    Color couleurR(103,244,177,150), couleurTitre(244, 249,178), couleurRSelect(103,244,177,255);

    Texture textureBackground;
    if (!textureBackground.loadFromFile("Ressources/fondMenu.jpg"))
        printf("erreur lors du chargement du background");
    textureBackground.setSmooth(true);
    Sprite spriteBackground(textureBackground);
    spriteBackground.scale(0.3f, 0.3f);


    while(continueMenu)
    {

        // Clear screen
        app.clear();

        // Fonction qui gere les evennement : fleche du bas, fleche du heut et entr�
        gestionEventMenu(app, choix, continueMenu, jouer);
        app.draw(spriteBackground);
        // Ecrit le nom du jeu qu'on a du entrer au dessus
        ecritText(nomJeu, MyFont, couleurTitre, 110, 10, -20, app);

        // on remet choix entre 1 et 3 vu qu'on a que 3 oprion dans le menu
        if (choix > 3)
            choix = 1;
        else if(choix < 1)
            choix = 3;

        // switch qui sert a avoir une opacit� differente suivant le choix : couleurSelect est plus opaque
        switch(choix)
        {

        case 1:
            ecritText(play, MyFont, couleurRSelect, 95, 230,250, app);
            ecritText(synopsys, MyFont, couleurR, 95, 230,350, app);
            ecritText(regles, MyFont, couleurR, 95, 230,450, app);
            break;
        case 2:
            ecritText(play, MyFont, couleurR, 95, 230,250, app);
            ecritText(synopsys, MyFont, couleurRSelect, 95, 230,350, app);
            ecritText(regles, MyFont, couleurR, 95, 230,450, app);
            break;
        case 3:
            ecritText(play, MyFont, couleurR, 95, 230,250, app);
            ecritText(synopsys, MyFont, couleurR, 95, 230,350, app);
            ecritText(regles, MyFont, couleurRSelect, 95, 230,450, app);
            break;

        }
        if (jouer != 4)
            jouer = choix;
        // Update the window
        app.display();
    }
    return jouer;
}

void gestionEventMenu(RenderWindow &app, int &choix, int &continuBoucle, int &play)
{

    // Process events
    Event event;
    while(app.pollEvent(event))
    {
        switch (event.type)
        {
        // fen�tre ferm�e
        case Event::Closed:
            app.close();
            continuBoucle = 0;
            play = 4;
            break;

        // touche press�e
        case Event::KeyPressed:
            if (event.key.code == Keyboard::Down)
                choix += 1;
            if (event.key.code == Keyboard::Up)
                choix -= 1;

            if (event.key.code == Keyboard::Enter)
                continuBoucle = 0;
            break;


        // we don't process other types of events
        default:
            break;
        }
    }
}

void regles(RenderWindow &app, Font &MyFont)
{
    int continuBoucle = 1;
    Texture textureBackground;
    if (!textureBackground.loadFromFile("Ressources/fondMenu.jpg"))
        printf("erreur lors du chargement du background");
    textureBackground.setSmooth(true);
    Sprite spriteBackground(textureBackground);
    spriteBackground.scale(0.3f, 0.3f);
    Color couleurR(0,0,0);
    Color couleurB(255,255,255);

    app.clear();
    app.draw(spriteBackground);

    char regle1[] = "Vous devez prot�ger votre slime, s'il meurt, c'est perdu";
    char regle2[] = "Les portes sont bloqu�s par des barreaux, trouver un autre chemin !";
    char regle3[] = "Vous ne pouvez plus retourner dans une salle compl�t�e";
    char regle4[] = "Vous avez 3 points de vie, s'il tombent � 0 c'est perdu";
    char regle5[] = "Les ennemis sont faibles, ils meurent en 1 coups, mais ils sont nombreux !";
    char regle6[] = "Les ennemis sont r�sistants, viser les jambes ne leur fera rien !";
    char regle7[] = "Controles :";
    char regle8[] = "Utilisez le fl�ches de votre clavier pour vous deplacer et espace pour attaquer !";

    ecritText(regle1, MyFont, couleurR, 45, 10,20, app);
    ecritText(regle2, MyFont, couleurR, 45, 10,120, app);
    ecritText(regle3, MyFont, couleurR, 45, 10,220, app);
    ecritText(regle4, MyFont, couleurR, 45, 10,320, app);
    ecritText(regle5, MyFont, couleurR, 45, 10,420, app);
    ecritText(regle6, MyFont, couleurR, 45, 10,520, app);
    ecritText(regle7, MyFont, couleurR, 90, 10,580, app);
    ecritText(regle8, MyFont, couleurB, 45, 10,680, app);

    app.display();
    Event event;

    while (continuBoucle == 1)
    {
        while(app.pollEvent(event))
        {
            switch (event.type)
            {
            // fen�tre ferm�e
            case Event::Closed:
                app.close();
                continuBoucle = 0;
                break;

            // touche press�e
            case Event::KeyPressed:
                if (event.key.code == Keyboard::Enter)
                    continuBoucle = 0;
                break;


            // we don't process other types of events
            default:
                break;
            }
        }
    }
}

void synopsis (RenderWindow &app, Font &MyFont)
{
    int continuBoucle = 1;

    Texture textureBackground;
    if (!textureBackground.loadFromFile("Ressources/fondMenu.jpg"))
        printf("erreur lors du chargement du background");
    textureBackground.setSmooth(true);
    Sprite spriteBackground(textureBackground);
    spriteBackground.scale(0.3f, 0.3f);
    Color couleurR(0,0,0);
    app.clear();
    app.draw(spriteBackground);
    char syn1[] = "Bienvenue dans mortal RESURECTION, un  rpg fantaisiste o� le but est";
    char syn2[] = "de sortir du donjon.";
    char syn3[] = "Le ninja s'est retrouv� coinc� dans le donjon, d�pouill� de toutes ses";
    char syn4[] = "armes.";
    char syn5[] = "Le ninja s'arme d'une lance pour regagner sa libert�.";
    ecritText(syn1, MyFont, couleurR, 45, 10,200, app);
    ecritText(syn2, MyFont, couleurR, 45, 10,250, app);
    ecritText(syn3, MyFont, couleurR, 45, 10,300, app);
    ecritText(syn4, MyFont, couleurR, 45, 10,350, app);
    ecritText(syn5, MyFont, couleurR, 45, 10,400, app);
    app.display();
    Event event;

    while (continuBoucle == 1)
    {
        while(app.pollEvent(event))
        {
            switch (event.type)
            {
            // fen�tre ferm�e
            case Event::Closed:
                app.close();
                continuBoucle = 0;
                break;

            // touche press�e
            case Event::KeyPressed:
                if (event.key.code == Keyboard::Enter)
                    continuBoucle = 0;
                break;


            // we don't process other types of events
            default:
                break;
            }
        }
    }
}

void deplacementMob(Entite &mob, Entite &player, Time dt, RenderWindow &app, int distanceMini)   //modification, le mob suis le joueur
{
    //------ D�placement ------//

    float deltaPlayerX = player.posX - (mob.posX+20); //distance entre player et mob en x
    float deltaPlayerY = player.posY - mob.posY; //distance entre player et mob en y

    float hyp = sqrt((deltaPlayerX * deltaPlayerX) + (deltaPlayerY * deltaPlayerY));



    if(hyp > distanceMini)
    {
        mob.speedX = (hyp / mob.speed) * deltaPlayerX;
        mob.speedY = (hyp / mob.speed) * deltaPlayerY;

        float dSpeed = sqrt(mob.speedX*mob.speedX + mob.speedY*mob.speedY);
        mob.speedX *= mob.speed/dSpeed;
        mob.speedY *= mob.speed/dSpeed;
    }
    else
    {
        mob.speedX = 0;
        mob.speedY = 0;
    }


    mob.posX += mob.speedX * dt.asSeconds();
    mob.posY += mob.speedY * dt.asSeconds();

    mob.sprite.setPosition(mob.posX, mob.posY);

}
void attaqueDuMob(Entite &player, Entite &mob, Entite &attaqueMob, Time dt, int numFrame)
{


    if( numFrame%100==0)
    {

        attaqueMob.posX = mob.posX + 50;
        attaqueMob.posY = mob.posY + 50;

        attaqueMob.speedX = player.posX + 20 - attaqueMob.posX;
        attaqueMob.speedY = player.posY  + 30 - attaqueMob.posY;


    }
    if(attaqueMob.posX > LARGEUR_FENETRE - 105 || attaqueMob.posY > HAUTEUR_FENETRE-105 || attaqueMob.posY < 105 || attaqueMob.posX < 105) //manque l'option "touche joueur"
    {
        attaqueMob.posX = -10;
        attaqueMob.posY = 0;
        attaqueMob.speedX = 0;
        attaqueMob.speedY = 0;
    }
    float dSpeed = sqrt(attaqueMob.speedX*attaqueMob.speedX + attaqueMob.speedY*attaqueMob.speedY);

    attaqueMob.speedX *= attaqueMob.speed/dSpeed;
    attaqueMob.speedY *= attaqueMob.speed/dSpeed ;

    attaqueMob.posX += attaqueMob.speedX * dt.asSeconds();
    attaqueMob.posY += attaqueMob.speedY * dt.asSeconds();
    attaqueMob.sprite.setPosition(attaqueMob.posX, attaqueMob.posY);

}


//Fonction pour afficher la vie du joueur en haut � gauche de l'�cran
void affichageVie(RenderWindow &app, Entite &player, Texture textureCoeurs[])
{
    Sprite spriteCoeur;

    switch (player.pointsdevie)
    {
    case 3 :
        spriteCoeur.setTexture(textureCoeurs[3]);
        break;
    case 2 :
        spriteCoeur.setTexture(textureCoeurs[2]);
        break;
    case 1 :
        spriteCoeur.setTexture(textureCoeurs[1]);
        break;
    case 0 :
        spriteCoeur.setTexture(textureCoeurs[0]);
        break;
    default :
        printf("Nb points de vie incorrect\n");
    }
    spriteCoeur.setPosition(0,0);
    app.draw(spriteCoeur);
}

void itemCoeur(Entite &player, Item &coeur, RenderWindow &app)
{
    if(player.posX+30 > coeur.posX && player.posX < coeur.posX + 30 && player.posY+60 > coeur.posY && player.posY < coeur.posY + 42)
    {
        if(player.pointsdevie < player.vieMax)
        {
            player.pointsdevie++;
            coeur.actif = 0;
        }
    }
    app.draw(coeur.sprite);
}

void deplacementPet( Entite &player, Entite &pet, Time dt, RenderWindow &app)
{
    //------ D�placement PET------//
    float deltaPlayerX = player.posX + 10- pet.posX; //distance entre player et mob en x
    float deltaPlayerY = player.posY + 50  - pet.posY; //distance entre player et mob en y

    float hyp = sqrt((deltaPlayerX * deltaPlayerX) + ((deltaPlayerY ) * (deltaPlayerY)));

    pet.speedX = (hyp / pet.speed) * deltaPlayerX ;
    pet.speedY = (hyp / pet.speed) * deltaPlayerY ;

    if(hyp < 20)
    {
        pet.speedX = 0;
        pet.speedY = 0;
    }
    //printf("%i", pet.pointsdevie);
    if (pet.speedX > 0)
        pet.sprite.setTexture(pet.texture[(pet.vieMax - pet.pointsdevie)*3 +1]);
    else if (pet.speedX < 0)
        pet.sprite.setTexture(pet.texture[(pet.vieMax - pet.pointsdevie)*3 + 2]);

    else
        pet.sprite.setTexture(pet.texture[(pet.vieMax - pet.pointsdevie)*3]);

    pet.posX += pet.speedX * dt.asSeconds();
    pet.posY += pet.speedY * dt.asSeconds();
    pet.sprite.setPosition(pet.posX, pet.posY);

}


void attaqueMobTouche(Entite &player,Entite &pet, Entite &attaqueMob)
{
    if (attaqueMob.posX > player.posX && attaqueMob.posX < player.posX + 30 && attaqueMob.posY > player.posY && attaqueMob.posY < player.posY + 80)
    {
        player.pointsdevie -= attaqueMob.degats;

        if (player.numTexture < 3)
            player.numTexture = 6;
        else
            player.numTexture = 7;

        attaqueMob.posX = 0;
        attaqueMob.posY = -10;
    }
    if (attaqueMob.posX > pet.posX && attaqueMob.posX < pet.posX + 25 && attaqueMob.posY > pet.posY && attaqueMob.posY < pet.posY + 25)
    {
        pet.pointsdevie -= attaqueMob.degats;

        attaqueMob.posX = 0;
        attaqueMob.posY = -10;
    }

}
void attaqueMobCACTouche(Entite &player,Entite &pet, Entite &mob, int numFrame)
{
    //gestion des textures
    if (numFrame % 10 == 0)
    {
        if (mob.speedX > 0)
        {
            mob.numTexture += 1;
            if(mob.numTexture > 2)
                mob.numTexture = 0;
        }
        else
        {
            mob.numTexture += 1;
            if(mob.numTexture < 3 || mob.numTexture > 5)
                mob.numTexture = 3;
        }
    }


    float deltaPlayerX = player.posX - (mob.posX +10) ; //distance entre player et mob en x
    float deltaPlayerY = player.posY  - (mob.posY +30); //distance entre player et mob en y
    float deltaPetX = pet.posX  - mob.posX; //distance entre player et mob en x
    float deltaPetY = pet.posY  - mob.posY; //distance entre player et mob en y

    float hypPet = sqrt((deltaPetX * deltaPetX) + ((deltaPetY ) * (deltaPetY)));
    float hypPlayer = sqrt((deltaPlayerX * deltaPlayerX) + ((deltaPlayerY ) * (deltaPlayerY)));
    if (numFrame % 30 == 0)
    {
        if ( hypPet < 40 )
        {
            pet.pointsdevie -= mob.degats;
            //mob.sprite.setTexture(mob.texture[6]);
            printf("pet touche\n");
        }
        if ( hypPlayer < 60 )
        {
            player.pointsdevie -= mob.degats;
            if (player.numTexture < 3)
                player.numTexture = 6;
            else
                player.numTexture = 7;
            printf("player touche\n");
        }
    }
}
void attaqueJoueurTouche(Entite &player, Entite &mob, Time &dtAttaque, Clock &clock)
{
    dtAttaque = clock.getElapsedTime();
    int attaque = 0;
    if (Keyboard::isKeyPressed(Keyboard::Space))
    {
        if (dtAttaque.asSeconds() > 0.7)
        {
            attaque = 1;
            dtAttaque= clock.restart();
        }
    }
    if (dtAttaque.asSeconds() < 0.3)
    {
        attaque = 1;
    }
    if(attaque)
    {
        if (mob.posY  < player.posY +20 && mob.posY + HAUTEUR_MOB >  player.posY +20)
        {
            if (player.numTexture < 3 && mob.posX  < player.posX +50 && mob.posX >  player.posX)
            {
                mob.pointsdevie -= player.degats;
            }


            if (player.numTexture > 2 && mob.posX  < player.posX && mob.posX + HAUTEUR_MOB >  player.posX - 50)
            {
                mob.pointsdevie -= player.degats;
            }
        }
    }


}
void attaqueDuPlayer(Entite &player, RenderWindow &app, Sprite &lanceDroiteSprite, Sprite &lanceGaucheSprite, Time &dtAttaque, Clock &clock)
{
    dtAttaque = clock.getElapsedTime();
    lanceDroiteSprite.setPosition(-60,0);
    lanceGaucheSprite.setPosition(-60,0);
    int attaque = 0;
    if (Keyboard::isKeyPressed(Keyboard::Space))
    {
        if (dtAttaque.asSeconds() > 0.7)
        {
            attaque = 1;
            dtAttaque= clock.restart();
        }
    }
    if (dtAttaque.asSeconds() < 0.3)
    {
        attaque = 1;
    }
    if(attaque)
    {
        if (player.numTexture > 2)
            lanceGaucheSprite.setPosition(player.posX - 45, player.posY + 30);

        if (player.numTexture < 3)
            lanceDroiteSprite.setPosition(player.posX + 27, player.posY + 30);
    }
}

void jeuDansSalle(RenderWindow &app, Salle &salle, Entite &player, Entite &pet, int &numSalle, Texture textureCoeurs[], int &mort)
{
    Clock clock, clockAttaque, clockAttaqueMobCAC;
    Time dt, dtAttaque, dtTempsProcheDuJoueur;
    dtAttaque = clockAttaque.restart();

    Entite mobTireur[salle.nbMobTireurs], attaqueMob[salle.nbMobTireurs];
    for (int i = 0; i < salle.nbMobTireurs; i++)
    {
        mobTireur[i].posX = salle.posInitMobTireurs[0][i];
        mobTireur[i].posY = salle.posInitMobTireurs[1][i];
        mobTireur[i].vieMax = 1;
        mobTireur[i].pointsdevie = mobTireur[i].vieMax;
        mobTireur[i].speed = 200;
        mobTireur[i].speedX = 0;
        mobTireur[i].speedY = 0;

        // Load sprite mobTireur
        if (!mobTireur[i].texture[0].loadFromFile("Ressources/mobs/slime1.png"))
            printf("erreur chargement image 1");

        mobTireur[i].sprite.setTexture(mobTireur[i].texture[0]);
        //mobTireur[i].sprite.scale(0.6f, 0.6f);
        mobTireur[i].sprite.setPosition(mobTireur[i].posX, mobTireur[i].posY);



        attaqueMob[i].posX = -10.0;
        attaqueMob[i].posY = 0.0;
        attaqueMob[i].speed = 500;
        attaqueMob[i].degats = 1;

        // Load sprite attaqueMob
        if (!attaqueMob[i].texture[0].loadFromFile("Ressources/mobs/attaque.png"))
            printf("erreur chargement image attaque");
        attaqueMob[i].sprite.setTexture(attaqueMob[i].texture[0]);
        attaqueMob[i].sprite.setPosition(attaqueMob[i].posX, attaqueMob[i].posY);

    }

    Entite mobCAC[salle.nbMobCAC];
    for (int i = 0; i < salle.nbMobCAC; i++)
    {
        mobCAC[i].posX = salle.posInitMobTireurs[0][i];
        mobCAC[i].posY = salle.posInitMobTireurs[1][i];
        mobCAC[i].vieMax = 1;
        mobCAC[i].pointsdevie = mobCAC[i].vieMax;
        mobCAC[i].speed = 100;
        mobCAC[i].speedX = 0;
        mobCAC[i].speedY = 0;
        mobCAC[i].degats = 1;
        mobCAC[i].numTexture = 0;

        for(int j = 0; j < 6; j++)
        {
            char nomTexture[30];
            sprintf(nomTexture,"Ressources/mobs/goblin/%i.png", j);
            // Load sprite mobCAC
            if (!mobCAC[i].texture[j].loadFromFile(nomTexture))
                printf("erreur chargement image goblin");
        }
        mobCAC[i].sprite.setTexture(mobCAC[i].texture[mobCAC[i].numTexture]);
        mobCAC[i].sprite.setPosition(mobCAC[i].posX, mobCAC[i].posY);
    }

    /*for (int i=0; i<8; i++)
    {
        char nomTexture[30];
        sprintf(nomTexture,"Ressources/mobs/goblin/%i.png", i);
        // Load le sprite du joueur
        if (!player.texture[i].loadFromFile(nomTexture))
            printf("erreur chargement image joueur");
    }
    player.sprite.setTexture(player.texture[1]);
    player.sprite.setPosition(player.posX, player.posY);*/

    player.posX = salle.spawnPlayer[0];
    player.posY = salle.spawnPlayer[1];
    pet.posX = player.posX+20;
    pet.posY = player.posY+50;

    // Load le sprite lance
    Texture lanceGaucheTexture;
    if (!lanceGaucheTexture.loadFromFile("Ressources/armes/lanceGauche.png"))
        printf("bug chargement image");
    Sprite lanceGaucheSprite(lanceGaucheTexture);

    Texture lanceDroiteTexture;
    if (!lanceDroiteTexture.loadFromFile("Ressources/armes/lanceDroite.png"))
        printf("bug chargement image");
    Sprite lanceDroiteSprite(lanceDroiteTexture);

    int numFrame = 0, jeu=1;
    int nbMobT = salle.nbMobTireurs;
    int nbMobC = salle.nbMobCAC;

    while(jeu)
    {
        numFrame++;
        if (numFrame >= 1200)
            numFrame = 0;

        dt = clock.restart();
        // Clear screen
        app.clear();

        gestionEventJeu(app, jeu, dt, player, numFrame); //menu
        for (int i = 0; i < salle.nbMobTireurs; i++)
        {
            if (mobTireur[i].pointsdevie > 0)
            {
                deplacementMob(mobTireur[i], player, dt, app, 250 + 30*i);
                attaqueDuMob(player, mobTireur[i], attaqueMob[i], dt, numFrame + 15*i);
                attaqueMobTouche(player, pet, attaqueMob[i]);
                attaqueJoueurTouche(player, mobTireur[i], dtAttaque, clockAttaque);
                if (mobTireur[i].pointsdevie == 0)
                    nbMobT -=1;
            }

        }
        for (int i = 0; i < salle.nbMobCAC; i++)
        {
            if (mobCAC[i].pointsdevie > 0)
            {
                deplacementMob(mobCAC[i], player, dt, app, 0);
                attaqueMobCACTouche(player, pet, mobCAC[i], numFrame);
                attaqueJoueurTouche(player, mobCAC[i], dtAttaque, clockAttaque);
                mobCAC[i].sprite.setTexture(mobCAC[i].texture[mobCAC[i].numTexture]);
                if (mobCAC[i].pointsdevie == 0)
                    nbMobC -=1;
            }
        }
        attaqueDuPlayer(player, app, lanceDroiteSprite, lanceGaucheSprite, dtAttaque, clockAttaque); //attaque du joueur

        deplacementPet(player, pet, dt, app);

        player.sprite.setTexture(player.texture[player.numTexture]);

        app.draw(salle.sprite);
        for (int i = 0; i < salle.nbMobTireurs; i++)
        {
            if (mobTireur[i].pointsdevie > 0)
            {
                app.draw(mobTireur[i].sprite);
                app.draw(attaqueMob[i].sprite);
            }

        }
        app.draw(player.sprite);
        app.draw(pet.sprite);
        for (int i = 0; i < salle.nbMobCAC; i++)
        {
            if (mobCAC[i].pointsdevie > 0)
            {
                app.draw(mobCAC[i].sprite);
            }

        }

        //attaqueMobCAC(mobCAC, nbMobCAC, player, app)
        app.draw(lanceDroiteSprite);
        app.draw(lanceGaucheSprite);

        affichageVie(app, player, textureCoeurs);
        if(salle.coeur.actif)
            itemCoeur(player, salle.coeur, app);


        // Update the window
        app.display();

        if (player.pointsdevie < 1 || pet.pointsdevie < 1)
        {
            jeu = 0;
            mort = 1;
        }
        if (player.posX > salle.sortie[0][0] && player.posX < salle.sortie[0][1] &&
                player.posY > salle.sortie[1][0] && player.posY < salle.sortie[1][1] && nbMobC == 0 && nbMobT == 0)
        {
            jeu = 0;
            numSalle++;
        }

    }



}

// foncion jeu comment� pck elle est faite a la rache
int jeu(RenderWindow &app, Font MyFont)
{
    int largeurBord = 130, numSalle =0;
    int posInitMob[2][4]= {largeurBord, LARGEUR_FENETRE-largeurBord, LARGEUR_FENETRE-largeurBord, largeurBord,
                           largeurBord, largeurBord, HAUTEUR_FENETRE-largeurBord, HAUTEUR_FENETRE-largeurBord
                          };

    int sortieDroite[2][2] = {1100, 1200,
                              250, 370
                             };
    int sortieGauche[2][2] = {20, 55,
                              250, 370
                             };
    int sortieHaut[2][2] = {540, 603,
                            10, 50
                           };

    int sortieBas[2][2] = {540, 603,
                           690, 710
                          };





    Salle salle[8];





    //----- Initialisation salle 0 -----//
    salle[0].nbMobCAC = 1;
    salle[0].nbMobTireurs = 0;
    salle[0].spawnPlayer[0]= 565;
    salle[0].spawnPlayer[1]= 90;

// Load le sprite de la map
    if (!salle[0].texture.loadFromFile("Ressources/map/map0.png"))
        printf("erreur chargement de la map");

    salle[0].sprite.setTexture(salle[0].texture);

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[0].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[0].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[0].sortie[i][j] = sortieDroite [i][j];
        }
    }





    //----- Initialisation salle 1 -----//
    salle[1].nbMobCAC = 2;
    salle[1].nbMobTireurs = 0;
    salle[1].spawnPlayer[0]= 100;
    salle[1].spawnPlayer[1]= 330;

    //item coeur
    if (!salle[1].coeur.texture.loadFromFile("Ressources/items/itemCoeur.png"))
        printf("erreur lors du chargement de l'item coeur");
    salle[1].coeur.texture.setSmooth(true);
    salle[1].coeur.sprite.setTexture(salle[1].coeur.texture);
    salle[1].coeur.posX = 1000;
    salle[1].coeur.posY = 600;
    salle[1].coeur.sprite.setPosition(salle[1].coeur.posX, salle[1].coeur.posY);

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[1].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[1].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[1].sortie[i][j] = sortieBas [i][j];
        }
    }
    if (!salle[1].texture.loadFromFile("Ressources/map/map2.png"))
        printf("erreur chargement de la map");
    salle[1].sprite.setTexture(salle[1].texture);

    //----- Initialisation salle 2 -----//
    salle[2].nbMobCAC = 0;
    salle[2].nbMobTireurs = 1;
    salle[2].spawnPlayer[0]= 564;
    salle[2].spawnPlayer[1]= 13;

    salle[2].posInitMobTireurs[0][0] = LARGEUR_FENETRE-largeurBord;
    salle[2].posInitMobTireurs[1][0] = HAUTEUR_FENETRE-largeurBord;


    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[2].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[2].sortie[i][j] = sortieDroite [i][j];
        }
    }
    if (!salle[2].texture.loadFromFile("Ressources/map/map4.png"))
        printf("erreur chargement de la map");

    salle[2].sprite.setTexture(salle[2].texture);


    //----- Initialisation salle 3 -----//
    salle[3].nbMobCAC = 2;
    salle[3].nbMobTireurs = 1;
    salle[3].spawnPlayer[0]= 100;
    salle[3].spawnPlayer[1]= 330;

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[3].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[3].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[3].sortie[i][j] = sortieHaut [i][j];
        }
    }

    if (!salle[3].texture.loadFromFile("Ressources/map/map1.png"))
        printf("erreur chargement de la map");

    salle[3].sprite.setTexture(salle[3].texture);

    //----- Initialisation salle 4 -----//
    salle[4].nbMobCAC = 3;
    salle[4].nbMobTireurs = 1;
    salle[4].spawnPlayer[0]= 571;
    salle[4].spawnPlayer[1]= 723;

    //item coeur
    if (!salle[4].coeur.texture.loadFromFile("Ressources/items/itemCoeur.png"))
        printf("erreur lors du chargement de l'item coeur");
    salle[4].coeur.texture.setSmooth(true);
    salle[4].coeur.sprite.setTexture(salle[4].coeur.texture);
    salle[4].coeur.posX = 1000;
    salle[4].coeur.posY = 150;
    salle[4].coeur.sprite.setPosition(salle[4].coeur.posX, salle[4].coeur.posY);

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[4].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[4].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[4].sortie[i][j] = sortieHaut [i][j];
        }
    }

    if (!salle[4].texture.loadFromFile("Ressources/map/map5.png"))
        printf("erreur chargement de la map");

    salle[4].sprite.setTexture(salle[4].texture);

    //----- Initialisation salle 5 -----//
    salle[5].nbMobCAC = 2;
    salle[5].nbMobTireurs = 2;
    salle[5].spawnPlayer[0]= 571;
    salle[5].spawnPlayer[1]= 723;

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[5].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[5].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[5].sortie[i][j] = sortieGauche [i][j];
        }
    }

    if (!salle[5].texture.loadFromFile("Ressources/map/map2.png"))
        printf("erreur chargement de la map");

    salle[5].sprite.setTexture(salle[5].texture);

    //----- Initialisation salle 6 -----//
    salle[6].nbMobCAC = 3;
    salle[6].nbMobTireurs = 3;
    salle[6].spawnPlayer[0]= 1158;
    salle[6].spawnPlayer[1]= 350;

    //item coeur
    if (!salle[6].coeur.texture.loadFromFile("Ressources/items/itemCoeur.png"))
        printf("erreur lors du chargement de l'item coeur");
    salle[6].coeur.texture.setSmooth(true);
    salle[6].coeur.sprite.setTexture(salle[6].coeur.texture);
//    salle[6].coeur.posX = ;
//    salle[6].coeur.posY = ;
    salle[6].coeur.sprite.setPosition(salle[6].coeur.posX, salle[6].coeur.posY);

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[6].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[6].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[6].sortie[i][j] = sortieGauche [i][j];
        }
    }

    if (!salle[6].texture.loadFromFile("Ressources/map/map6.png"))
        printf("erreur chargement de la map");

    salle[6].sprite.setTexture(salle[6].texture);

    //----- Initialisation salle 7 -----//
    salle[7].nbMobCAC = 4;
    salle[7].nbMobTireurs = 4;
    salle[7].spawnPlayer[0]= 1158;
    salle[7].spawnPlayer[1]= 350;

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[7].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[7].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[7].sortie[i][j] = sortieGauche [i][j];
        }
    }

    if (!salle[7].texture.loadFromFile("Ressources/map/map3.png"))
        printf("erreur chargement de la map");

    salle[7].sprite.setTexture(salle[7].texture);



    Entite player;

    player.pointsdevie = player.vieMax;
    player.numTexture = 3;
    player.degats = 1;

    Entite pet;
    pet.speed = 50;
    pet.vieMax = 5;
    pet.pointsdevie = pet.vieMax;

    // Load le sprite du joueur
    for (int i=0; i<8; i++)
    {
        char nomTexture[30];
        sprintf(nomTexture,"Ressources/perso/sprite_%i.png", i);
        // Load le sprite du joueur
        if (!player.texture[i].loadFromFile(nomTexture))
            printf("erreur chargement image joueur");
    }
    player.sprite.setTexture(player.texture[1]);
    player.sprite.setPosition(player.posX, player.posY);

    for (int i=0; i<pet.vieMax*3+1; i++)
    {
        char nomTexture[30];
        sprintf(nomTexture,"Ressources/pet/sprite_%i.png", i);
        // Load le sprite du pet
        if (!pet.texture[i].loadFromFile(nomTexture))
            printf("erreur chargement image pet");
    }
    // Load sprite pet
    pet.sprite.setTexture(pet.texture[0]);
    pet.sprite.scale(2.0f, 2.0f);
    pet.sprite.setPosition(pet.posX, pet.posY);

    //Load la texture des coeurs pour les affichers
    Texture textureCoeurs[5];
    for (int i=0; i<4; i++)
    {
        char nomTexture[30];
        sprintf(nomTexture,"Ressources/items/%iCoeurs.png", i);
        // Load le sprite du joueur
        if (!textureCoeurs[i].loadFromFile(nomTexture))
            printf("erreur chargement image coeur");
    }

    //char nomJeu[] = "Fonction Jeu a faire";                                               //SUPPRESSION TEXTE ?????
    //Color couleurR(255,0,0,150), couleurB(0,0,255), couleurRSelect(255,0,0,255);
    int mode = 0;
    int mort = 0;
    while (!mort)
    {
        jeuDansSalle(app, salle[numSalle], player, pet, numSalle, textureCoeurs, mort);
        if (numSalle > 7)
        {
            mode = 5;
            break;
        }
    }
    if (mort)
        mode = 4;

    return mode;
}

void gestionEventJeu(RenderWindow &app, int &continuBoucle, Time dt, Entite &player, int numFrame)
{


    // Process events
    Event event;
    while(app.pollEvent(event))
    {
        switch (event.type)
        {
        // fen�tre ferm�e
        case Event::Closed:
            app.close();
            continuBoucle = 0;
            break;

        // touche press�e
        case Event::KeyPressed:

            if (event.key.code == Keyboard::Enter)
                continuBoucle = 0;
            break;


        // we don't process other types of events
        default:
            break;
        }
    }
    if (Keyboard::isKeyPressed(Keyboard::Left)) // Fleche gauche appuyee
    {
        if (player.numTexture < 3)
            player.numTexture = 3;

        if (player.speedX > (-player.speed))
            player.speedX -= ACCELERATION;

        else
            player.speedX = (-player.speed);
    }
    else
    {
        if (player.speedX < -100)
            player.speedX += ACCELERATION;

        else if (player.speedX < 0)
            player.speedX += ACCELERATION;

    }
    if (Keyboard::isKeyPressed(Keyboard::Right)) // Fleche droite appuyee
    {
        if (player.numTexture >= 3)
            player.numTexture = 0;
        if (player.speedX < player.speed)
            player.speedX += ACCELERATION;

        else
            player.speedX = player.speed;
    }
    else
    {
        if (player.speedX > 100)
            player.speedX -= ACCELERATION;
        else if (player.speedX > 0)
            player.speedX -= ACCELERATION;

    }

    if (Keyboard::isKeyPressed(Keyboard::Up)) // Fleche du haut appuyee
    {
        if (player.speedY > (-player.speed))
            player.speedY -= ACCELERATION;

        else
            player.speedY = (-player.speed);
    }
    else
    {
        if (player.speedY < -100)
            player.speedY += ACCELERATION;
        else if (player.speedY < 0)
            player.speedY += ACCELERATION;
    }
    if (Keyboard::isKeyPressed(Keyboard::Down)) // Fleche du bas appuyee
    {
        if (player.speedY < player.speed)
            player.speedY += ACCELERATION;

        else
            player.speedY = player.speed;
    }
    else
    {
        if (player.speedY > 100)
            player.speedY -= ACCELERATION;
        else if (player.speedY > 0)
            player.speedY -= ACCELERATION;

    }
    if (player.speedX < ACCELERATION - 1 && player.speedX > - ACCELERATION +1)
        player.speedX = 0;
    if (player.speedY < ACCELERATION - 1 && player.speedY > - ACCELERATION +1)
        player.speedY = 0;

    if (player.speedX > 0 && numFrame % 12 == 0)
    {
        player.numTexture += 1;
        if (player.numTexture > 2)
            player.numTexture = 0;
    }
    else if(player.speedX < 0 && numFrame % 12 == 0)
    {
        player.numTexture += 1;
        if (player.numTexture > 5)
            player.numTexture = 3;
    }

    if (player.speedX == 0)
    {

        if (player.numTexture > 2)
        {

            player.numTexture = 3;
        }


        if (player.numTexture < 3)
            player.numTexture = 0;
    }

    if (player.posX > LARGEUR_FENETRE - 130 && player.speedX > 0 && (player.posY < 270 || player.posY > 370))
        player.speedX = 0;


    else if (player.posX < 100 && player.speedX < 0 && ( player.posY < 340 || player.posY > 370))
        player.speedX = 0;

    if (player.posY > HAUTEUR_FENETRE - 155 && player.speedY > 0 && (player.posX < 535 || player.posX > 590))

        player.speedY = 0;

    else if (player.posY < 50 && player.speedY < 0 && (player.posX < 535 || player.posX > 590))
        player.speedY = 0;


    if(player.posX > LARGEUR_FENETRE - 60 && player.speedX >= 0 || (player.posY < 25 && player.speedY <= 0))
    {
        player.speedX = 0;
        player.speedY = 0;
    }
    else if ((player.posY > HAUTEUR_FENETRE - 90 && player.speedY >= 0) || player.posX < 50 && player.speedX <= 0)
    {
        player.speedX = 0;
        player.speedY = 0;
    }

    float dSpeed = sqrt(player.speedX*player.speedX + player.speedY*player.speedY);

    if (dSpeed > player.speed)
    {
        player.speedX *= player.speed/dSpeed;
        player.speedY *= player.speed/dSpeed ;
    }
    player.posX += player.speedX *dt.asSeconds();

    player.posY += player.speedY *dt.asSeconds();

    player.sprite.setPosition(player.posX, player.posY);
}
