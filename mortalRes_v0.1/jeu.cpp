#include <SFML/Graphics.hpp>
#include "fonction.hpp"

void jeuDansSalle(RenderWindow &app, Salle &salle, Entite &player, Entite &pet, int &numSalle, Texture textureCoeurs[], int &mort)
{
    Clock clock, clockAttaque, clockAttaqueMobCAC;
    Time dt, dtAttaque, dtTempsProcheDuJoueur;
    dtAttaque = clockAttaque.restart();

    Entite mobTireur[salle.nbMobTireurs], attaqueMob[salle.nbMobTireurs];
    for (int i = 0; i < salle.nbMobTireurs; i++)
    {
        mobTireur[i].posX = salle.posInitMobTireurs[0][i];
        mobTireur[i].posY = salle.posInitMobTireurs[1][i];
        mobTireur[i].vieMax = 1;
        mobTireur[i].pointsdevie = mobTireur[i].vieMax;
        mobTireur[i].speed = 200;
        mobTireur[i].speedX = 0;
        mobTireur[i].speedY = 0;

        // Load sprite mobTireur
        if (!mobTireur[i].texture[0].loadFromFile("Ressources/mobs/slime1.png"))
            printf("erreur chargement image 1");

        mobTireur[i].sprite.setTexture(mobTireur[i].texture[0]);
        //mobTireur[i].sprite.scale(0.6f, 0.6f);
        mobTireur[i].sprite.setPosition(mobTireur[i].posX, mobTireur[i].posY);



        attaqueMob[i].posX = -10.0;
        attaqueMob[i].posY = 0.0;
        attaqueMob[i].speed = 500;
        attaqueMob[i].degats = 1;

        // Load sprite attaqueMob
        if (!attaqueMob[i].texture[0].loadFromFile("Ressources/mobs/attaque.png"))
            printf("erreur chargement image attaque");
        attaqueMob[i].sprite.setTexture(attaqueMob[i].texture[0]);
        attaqueMob[i].sprite.setPosition(attaqueMob[i].posX, attaqueMob[i].posY);

    }

    Entite mobCAC[salle.nbMobCAC];
    for (int i = 0; i < salle.nbMobCAC; i++)
    {
        mobCAC[i].posX = salle.posInitMobTireurs[0][i];
        mobCAC[i].posY = salle.posInitMobTireurs[1][i];
        mobCAC[i].vieMax = 1;
        mobCAC[i].pointsdevie = mobCAC[i].vieMax;
        mobCAC[i].speed = 100;
        mobCAC[i].speedX = 0;
        mobCAC[i].speedY = 0;
        mobCAC[i].degats = 1;
        mobCAC[i].numTexture = 0;

        for(int j = 0; j < 10; j++)
        {
            char nomTexture[30];
            sprintf(nomTexture,"Ressources/mobs/goblin/%i.png", j);
            // Load sprite mobCAC
            if (!mobCAC[i].texture[j].loadFromFile(nomTexture))
                printf("erreur chargement image goblin");
        }
        mobCAC[i].sprite.setTexture(mobCAC[i].texture[mobCAC[i].numTexture]);
        mobCAC[i].sprite.setPosition(mobCAC[i].posX, mobCAC[i].posY);
    }

    /*for (int i=0; i<8; i++)
    {
        char nomTexture[30];
        sprintf(nomTexture,"Ressources/mobs/goblin/%i.png", i);
        // Load le sprite du joueur
        if (!player.texture[i].loadFromFile(nomTexture))
            printf("erreur chargement image joueur");
    }
    player.sprite.setTexture(player.texture[1]);
    player.sprite.setPosition(player.posX, player.posY);*/

    player.posX = salle.spawnPlayer[0];
    player.posY = salle.spawnPlayer[1];
    pet.posX = player.posX+20;
    pet.posY = player.posY+50;

    int numFrame = 0, jeu=1;
    int nbMobT = salle.nbMobTireurs;
    int nbMobC = salle.nbMobCAC;

    while(jeu)
    {
        numFrame++;
        if (numFrame >= 1200)
            numFrame = 0;

        dt = clock.restart();
        // Clear screen
        app.clear();

        gestionEventJeu(app, jeu, dt, player, numFrame); //menu
        for (int i = 0; i < salle.nbMobTireurs; i++)
        {
            if (mobTireur[i].pointsdevie > 0)
            {
                deplacementMob(mobTireur[i], player, dt, app, 250 + 30*i);
                attaqueDuMob(player, mobTireur[i], attaqueMob[i], dt, numFrame + 15*i);
                if (player.arme.utilise == 1)
                    attaqueMobTouche(player, pet, attaqueMob[i]);
                attaqueJoueurTouche(player, mobTireur[i], dtAttaque, clockAttaque);
                if (mobTireur[i].pointsdevie == 0)
                    nbMobT -=1;
            }

        }
        for (int i = 0; i < salle.nbMobCAC; i++)
        {
            if (mobCAC[i].pointsdevie > 0)
            {
                deplacementMob(mobCAC[i], player, dt, app, 0);
                attaqueMobCACTouche(player, pet, mobCAC[i], numFrame);
                if (player.arme.utilise == 1)
                    attaqueJoueurTouche(player, mobCAC[i], dtAttaque, clockAttaque);
                mobCAC[i].sprite.setTexture(mobCAC[i].texture[mobCAC[i].numTexture]);
                if (mobCAC[i].pointsdevie == 0)
                    nbMobC -=1;
            }
        }

        app.draw(salle.sprite);
        if (player.arme.utilise == 1)
            attaqueDuPlayer(player, app, dtAttaque, clockAttaque); //attaque du joueur

        deplacementPet(player, pet, dt, app);

        player.sprite.setTexture(player.texture[player.numTexture]);


        for (int i = 0; i < salle.nbMobTireurs; i++)
        {
            if (mobTireur[i].pointsdevie > 0)
            {
                app.draw(mobTireur[i].sprite);
                app.draw(attaqueMob[i].sprite);
            }

        }
        app.draw(player.sprite);
        app.draw(pet.sprite);
        for (int i = 0; i < salle.nbMobCAC; i++)
        {
            if (mobCAC[i].pointsdevie > 0)
            {
                app.draw(mobCAC[i].sprite);
            }

        }

        //attaqueMobCAC(mobCAC, nbMobCAC, player, app)
        if (!player.arme.utilise)
            app.draw(salle.lance.sprite);

        affichageVie(app, player, textureCoeurs);
        if(salle.coeur.actif)
            itemCoeur(player, salle.coeur, app);
        if(salle.cookie.actif)
            itemCookie(pet, salle.cookie, app);
        if (!salle.lance.utilise)
            armeLance(player,salle.lance,app);




        // Update the window
        app.display();

        if (player.pointsdevie < 1 || pet.pointsdevie < 1)
        {
            jeu = 0;
            mort = 1;
        }
        if (player.posX > salle.sortie[0][0] && player.posX < salle.sortie[0][1] &&
                player.posY > salle.sortie[1][0] && player.posY < salle.sortie[1][1] && nbMobC == 0 && nbMobT == 0)
        {
            jeu = 0;
            numSalle++;
        }

    }



}

// foncion jeu comment� pck elle est faite a la rache
int jeu(RenderWindow &app, Font MyFont)
{
    int largeurBord = 150, numSalle =0;
    int posInitMob[2][4]= {largeurBord, LARGEUR_FENETRE-largeurBord, LARGEUR_FENETRE-largeurBord, largeurBord,
                           largeurBord, largeurBord, HAUTEUR_FENETRE-largeurBord, HAUTEUR_FENETRE-largeurBord
                          };

    int sortieDroite[2][2] = {1100, 1200,
                              250, 370};

    int sortieGauche[2][2] = {20, 55,
                              250, 370};

    int sortieHaut[2][2] = {540, 603,
                            10, 50};

    int sortieBas[2][2] = {530, 603,
                           690, 710};



    Texture textureCoeur, textureCookie;

    if (!textureCoeur.loadFromFile("Ressources/items/itemCoeur.png"))
        printf("erreur lors du chargement de l'item coeur");
    textureCoeur.setSmooth(true);

    if (!textureCookie.loadFromFile("Ressources/items/cookie.png"))
        printf("erreur lors du chargement de l'item coeur");
    textureCookie.setSmooth(true);


    Salle salle[8];

    //----- Initialisation salle 0 -----//
    salle[0].nbMobCAC = 1;
    salle[0].nbMobTireurs = 0;
    salle[0].spawnPlayer[0]= 565;
    salle[0].spawnPlayer[1]= 90;

// Load le sprite de la map
    if (!salle[0].texture.loadFromFile("Ressources/map/map0.png"))
        printf("erreur chargement de la map");


    salle[0].sprite.setTexture(salle[0].texture);

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[0].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[0].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[0].sortie[i][j] = sortieDroite [i][j];
        }
    }

    // Load le sprite lance

    if (!salle[0].lance.texture[0].loadFromFile("Ressources/armes/lanceGauche.png"))
        printf("bug chargement lance");
    salle[0].lance.sprite.setTexture(salle[0].lance.texture[0]);

    if (!salle[0].lance.texture[1].loadFromFile("Ressources/armes/lanceDroite.png"))
        printf("bug chargement image lance");
    salle[0].lance.sprite.setTexture(salle[0].lance.texture[1]);

    salle[0].lance.posX = LARGEUR_FENETRE/2;
    salle[0].lance.posY = HAUTEUR_FENETRE/2;
    salle[0].lance.sprite.setPosition(salle[0].lance.posX, salle[0].lance.posY);

    //----- Initialisation salle 1 -----//
    salle[1].nbMobCAC = 2;
    salle[1].nbMobTireurs = 0;
    salle[1].spawnPlayer[0]= 100;
    salle[1].spawnPlayer[1]= 330;

    //item coeur
    salle[1].coeur.sprite.setTexture(textureCoeur);
    salle[1].coeur.posX = 1000;
    salle[1].coeur.posY = 600;
    salle[1].coeur.sprite.setPosition(salle[1].coeur.posX, salle[1].coeur.posY);
    salle[1].coeur.actif = 1;


    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[1].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[1].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[1].sortie[i][j] = sortieBas [i][j];
        }
    }
    if (!salle[1].texture.loadFromFile("Ressources/map/map2.png"))
        printf("erreur chargement de la map");
    salle[1].sprite.setTexture(salle[1].texture);

    //----- Initialisation salle 2 -----//
    salle[2].nbMobCAC = 0;
    salle[2].nbMobTireurs = 1;
    salle[2].spawnPlayer[0]= 564;
    salle[2].spawnPlayer[1]= 13;

    salle[2].posInitMobTireurs[0][0] = LARGEUR_FENETRE-largeurBord;
    salle[2].posInitMobTireurs[1][0] = HAUTEUR_FENETRE-largeurBord;


    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[2].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[2].sortie[i][j] = sortieDroite [i][j];
        }
    }
    if (!salle[2].texture.loadFromFile("Ressources/map/map4.png"))
        printf("erreur chargement de la map");

    salle[2].sprite.setTexture(salle[2].texture);




    //----- Initialisation salle 3 -----//
    salle[3].nbMobCAC = 2;
    salle[3].nbMobTireurs = 1;
    salle[3].spawnPlayer[0]= 100;
    salle[3].spawnPlayer[1]= 330;

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[3].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[3].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[3].sortie[i][j] = sortieHaut [i][j];
        }
    }

    if (!salle[3].texture.loadFromFile("Ressources/map/map1.png"))
        printf("erreur chargement de la map");

    salle[3].sprite.setTexture(salle[3].texture);

    //item Cookie
    salle[3].cookie.sprite.setTexture(textureCookie);
    salle[3].cookie.posX = 300;
    salle[3].cookie.posY = 300;
    salle[3].cookie.sprite.setPosition(salle[3].cookie.posX, salle[3].cookie.posY);
    salle[3].cookie.actif =1;

    //----- Initialisation salle 4 -----//
    salle[4].nbMobCAC = 3;
    salle[4].nbMobTireurs = 1;
    salle[4].spawnPlayer[0]= 571;
    salle[4].spawnPlayer[1]= 723;

    //item coeur
    salle[4].coeur.sprite.setTexture(textureCoeur);
    salle[4].coeur.posX = 1000;
    salle[4].coeur.posY = 150;
    salle[4].coeur.sprite.setPosition(salle[4].coeur.posX, salle[4].coeur.posY);
    salle[4].coeur.actif =1;

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[4].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[4].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[4].sortie[i][j] = sortieHaut [i][j];
        }
    }

    if (!salle[4].texture.loadFromFile("Ressources/map/map5.png"))
        printf("erreur chargement de la map");

    salle[4].sprite.setTexture(salle[4].texture);

    //----- Initialisation salle 5 -----//
    salle[5].nbMobCAC = 2;
    salle[5].nbMobTireurs = 2;
    salle[5].spawnPlayer[0]= 571;
    salle[5].spawnPlayer[1]= 723;

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[5].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[5].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[5].sortie[i][j] = sortieGauche [i][j];
        }
    }

    if (!salle[5].texture.loadFromFile("Ressources/map/map2.png"))
        printf("erreur chargement de la map");

    salle[5].sprite.setTexture(salle[5].texture);

    //item Cookie
    salle[5].cookie.sprite.setTexture(textureCookie);
    salle[5].cookie.posX = 300;
    salle[5].cookie.posY = 300;
    salle[5].cookie.sprite.setPosition(salle[5].cookie.posX, salle[5].cookie.posY);
    salle[5].cookie.actif = 1;

    //----- Initialisation salle 6 -----//
    salle[6].nbMobCAC = 3;
    salle[6].nbMobTireurs = 3;
    salle[6].spawnPlayer[0]= 1158;
    salle[6].spawnPlayer[1]= 350;

    //item coeur
    salle[6].coeur.sprite.setTexture(textureCoeur);
//    salle[6].coeur.posX = ;
//    salle[6].coeur.posY = ;
    salle[6].coeur.sprite.setPosition(salle[6].coeur.posX, salle[6].coeur.posY);

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[6].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[6].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[6].sortie[i][j] = sortieGauche [i][j];
        }
    }

    if (!salle[6].texture.loadFromFile("Ressources/map/map6.png"))
        printf("erreur chargement de la map");

    salle[6].sprite.setTexture(salle[6].texture);

    salle[6].coeur.sprite.setTexture(textureCoeur);
    salle[6].coeur.posX = 1000;
    salle[6].coeur.posY = 600;
    salle[6].coeur.sprite.setPosition(salle[1].coeur.posX, salle[1].coeur.posY);
    salle[6].coeur.actif = 1;

    salle[6].cookie.sprite.setTexture(textureCookie);
    salle[6].cookie.posX = 300;
    salle[6].cookie.posY = 300;
    salle[6].cookie.sprite.setPosition(salle[5].cookie.posX, salle[5].cookie.posY);
    salle[6].cookie.actif = 1;

    //----- Initialisation salle 7 -----//
    salle[7].nbMobCAC = 4;
    salle[7].nbMobTireurs = 4;
    salle[7].spawnPlayer[0]= 1158;
    salle[7].spawnPlayer[1]= 350;

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[7].posInitMobTireurs[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for(int j=0; j<4; j++)
            salle[7].posInitMobCAC[i][j] = posInitMob[i][j];
    }

    for (int i = 0; i<2; i++)
    {
        for (int j = 0; j<2; j++)
        {
            salle[7].sortie[i][j] = sortieGauche [i][j];
        }
    }

    if (!salle[7].texture.loadFromFile("Ressources/map/map3.png"))
        printf("erreur chargement de la map");

    salle[7].sprite.setTexture(salle[7].texture);



    Entite player;

    player.pointsdevie = player.vieMax;
    player.numTexture = 3;
    player.degats = 1;

    Entite pet;
    pet.speed = 50;
    pet.vieMax = 5;
    pet.pointsdevie = pet.vieMax;

    // Load le sprite du joueur
    for (int i=0; i<8; i++)
    {
        char nomTexture[30];
        sprintf(nomTexture,"Ressources/perso/sprite_%i.png", i);
        // Load le sprite du joueur
        if (!player.texture[i].loadFromFile(nomTexture))
            printf("erreur chargement image joueur");
    }
    player.sprite.setTexture(player.texture[1]);
    player.sprite.setPosition(player.posX, player.posY);

    for (int i=0; i<pet.vieMax*3+1; i++)
    {
        char nomTexture[30];
        sprintf(nomTexture,"Ressources/pet/sprite_%i.png", i);
        // Load le sprite du pet
        if (!pet.texture[i].loadFromFile(nomTexture))
            printf("erreur chargement image pet");
    }
    // Load sprite pet
    pet.sprite.setTexture(pet.texture[0]);
    pet.sprite.scale(2.0f, 2.0f);
    pet.sprite.setPosition(pet.posX, pet.posY);

    //Load la texture des coeurs pour les affichers
    Texture textureCoeurs[5];
    for (int i=0; i<4; i++)
    {
        char nomTexture[30];
        sprintf(nomTexture,"Ressources/items/%iCoeurs.png", i);
        // Load le sprite du joueur
        if (!textureCoeurs[i].loadFromFile(nomTexture))
            printf("erreur chargement image coeur");
    }

    //char nomJeu[] = "Fonction Jeu a faire";                                               //SUPPRESSION TEXTE ?????
    //Color couleurR(255,0,0,150), couleurB(0,0,255), couleurRSelect(255,0,0,255);
    int mode = 0;
    int mort = 0;
    while (!mort)
    {
        jeuDansSalle(app, salle[numSalle], player, pet, numSalle, textureCoeurs, mort);
        if (numSalle > 7)
        {
            mode = 5;
            break;
        }
    }
    if (mort)
        mode = 4;

    return mode;
}


//Fonction pour afficher la vie du joueur en haut � gauche de l'�cran
void affichageVie(RenderWindow &app, Entite &player, Texture textureCoeurs[])
{
    Sprite spriteCoeur;

    switch (player.pointsdevie)
    {
    case 3 :
        spriteCoeur.setTexture(textureCoeurs[3]);
        break;
    case 2 :
        spriteCoeur.setTexture(textureCoeurs[2]);
        break;
    case 1 :
        spriteCoeur.setTexture(textureCoeurs[1]);
        break;
    case 0 :
        spriteCoeur.setTexture(textureCoeurs[0]);
        break;
    default :
        printf("Nb points de vie incorrect\n");
    }
    spriteCoeur.setPosition(0,0);
    app.draw(spriteCoeur);
}
