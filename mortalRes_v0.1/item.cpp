#include <SFML/Graphics.hpp>
#include "fonction.hpp"

void itemCoeur(Entite &player, Item &coeur, RenderWindow &app)
{
    if(player.posX+30 > coeur.posX && player.posX < coeur.posX + 30 && player.posY+60 > coeur.posY && player.posY < coeur.posY + 42)
    {
        if(player.pointsdevie < player.vieMax)
        {
            player.pointsdevie++;
            coeur.actif = 0;
        }
    }
    app.draw(coeur.sprite);
}

void itemCookie(Entite &pet, Item &cookie, RenderWindow &app)
{
    if(pet.posX+30 > cookie.posX && pet.posX < cookie.posX + 30 && pet.posY+60 > cookie.posY && pet.posY < cookie.posY + 42)
    {
        if(pet.pointsdevie < pet.vieMax)
        {
            pet.pointsdevie++;
            cookie.actif -= 1;
        }
    }
    app.draw(cookie.sprite);
}
void armeLance(Entite &player, Arme &lance, RenderWindow &app)
{
    if(player.posX+30 > lance.posX && player.posX < lance.posX + 30 && player.posY+60 > lance.posY && player.posY < lance.posY + 42)
    {
        lance.utilise = 1;
        player.arme.utilise = 1;
        player.arme.texture[0] = lance.texture[0];
        player.arme.texture[1] = lance.texture[1];
    }
    app.draw(lance.sprite);
}
