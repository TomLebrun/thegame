#include <SFML/Graphics.hpp>
#include "fonction.hpp"
#include <math.h>


void deplacementMob(Entite &mob, Entite &player, Time dt, RenderWindow &app, int distanceMini)   //modification, le mob suis le joueur
{
    //------ Déplacement ------//


    int constX = 0;
    if (mob.speedX > 0)
        constX = 70;

    float deltaPlayerX = player.posX - (mob.posX + constX); //distance entre player et mob en x
    float deltaPlayerY = player.posY - (mob.posY); //distance entre player et mob en y

    float hyp = sqrt((deltaPlayerX * deltaPlayerX) + (deltaPlayerY * deltaPlayerY));



    if(hyp > distanceMini)
    {
        mob.speedX = (hyp / mob.speed) * deltaPlayerX;
        mob.speedY = (hyp / mob.speed) * deltaPlayerY;

        float dSpeed = sqrt(mob.speedX*mob.speedX + mob.speedY*mob.speedY);
        mob.speedX *= mob.speed/dSpeed;
        mob.speedY *= mob.speed/dSpeed;
    }
    else
    {
        mob.speedX = 0;
        mob.speedY = 0;
    }


    mob.posX += mob.speedX * dt.asSeconds();
    mob.posY += mob.speedY * dt.asSeconds();

    mob.sprite.setPosition(mob.posX, mob.posY);

}
void attaqueDuMob(Entite &player, Entite &mob, Entite &attaqueMob, Time dt, int numFrame)
{


    if( numFrame%100==0)
    {

        attaqueMob.posX = mob.posX + 50;
        attaqueMob.posY = mob.posY + 50;

        attaqueMob.speedX = player.posX + 20 - attaqueMob.posX;
        attaqueMob.speedY = player.posY  + 30 - attaqueMob.posY;


    }
    if(attaqueMob.posX > LARGEUR_FENETRE - 60 || attaqueMob.posY > HAUTEUR_FENETRE-60 || attaqueMob.posY < 60 || attaqueMob.posX < 60) //manque l'option "touche joueur"
    {
        attaqueMob.posX = -10;
        attaqueMob.posY = 0;
        attaqueMob.speedX = 0;
        attaqueMob.speedY = 0;
    }
    float dSpeed = sqrt(attaqueMob.speedX*attaqueMob.speedX + attaqueMob.speedY*attaqueMob.speedY);

    attaqueMob.speedX *= attaqueMob.speed/dSpeed;
    attaqueMob.speedY *= attaqueMob.speed/dSpeed ;

    attaqueMob.posX += attaqueMob.speedX * dt.asSeconds();
    attaqueMob.posY += attaqueMob.speedY * dt.asSeconds();
    attaqueMob.sprite.setPosition(attaqueMob.posX, attaqueMob.posY);

}

void attaqueMobTouche(Entite &player,Entite &pet, Entite &attaqueMob)
{
    if (attaqueMob.posX > player.posX && attaqueMob.posX < player.posX + 30 && attaqueMob.posY > player.posY && attaqueMob.posY < player.posY + 80)
    {
        player.pointsdevie -= attaqueMob.degats;

        if (player.numTexture < 3)
            player.numTexture = 6;
        else
            player.numTexture = 7;

        attaqueMob.posX = 0;
        attaqueMob.posY = -10;
    }
    if (attaqueMob.posX > pet.posX && attaqueMob.posX < pet.posX + 25 && attaqueMob.posY > pet.posY && attaqueMob.posY < pet.posY + 25)
    {
        pet.pointsdevie -= attaqueMob.degats;

        attaqueMob.posX = 0;
        attaqueMob.posY = -10;
    }

}
void attaqueMobCACTouche(Entite &player,Entite &pet, Entite &mob, int numFrame)
{
    //gestion des textures
    if (numFrame % 10 == 0)
    {
        if (mob.speedX >=0)
        {
            if (mob.numTexture == 6)
                mob.numTexture = 7;
            else
            {
                mob.numTexture += 1;
                if(mob.numTexture > 2)
                    mob.numTexture = 0;
            }

        }
        else
        {
            if (mob.numTexture == 8)
                mob.numTexture = 9;
            else
            {
                mob.numTexture += 1;
                if(mob.numTexture < 3 || mob.numTexture > 5)
                    mob.numTexture = 3;
            }
        }
    }
    int constX = 0;
    if (mob.speedX > 0)
        constX = 60;

    float deltaPlayerX = player.posX - (mob.posX + constX) ; //distance entre player et mob en x
    float deltaPlayerY = player.posY  - (mob.posY +30); //distance entre player et mob en y
    float deltaPetX = pet.posX  - (mob.posX + constX); //distance entre player et mob en x
    float deltaPetY = pet.posY  - (mob.posY + 60); //distance entre player et mob en y

    float hypPet = sqrt((deltaPetX * deltaPetX) + ((deltaPetY ) * (deltaPetY)));
    float hypPlayer = sqrt((deltaPlayerX * deltaPlayerX) + ((deltaPlayerY ) * (deltaPlayerY)));
    if (numFrame % 30 == 0)
    {
        if ( hypPet < 40 )
        {
            if (mob.speedX > 0)
            {
                mob.numTexture = 6;
            }
            else
            {
                mob.numTexture = 8;
            }
            pet.pointsdevie -= mob.degats;
            //mob.sprite.setTexture(mob.texture[6]);
            printf("pet touche\n");
        }
        if ( hypPlayer < 60 )
        {
            if (mob.speedX > 0)
            {
                mob.numTexture = 6;
            }
            else
            {
                mob.numTexture = 8;
            }

            player.pointsdevie -= mob.degats;
            if (player.numTexture < 3)
                player.numTexture = 6;
            else
                player.numTexture = 7;
            printf("player touche\n");
        }
    }
}

void deplacementPet( Entite &player, Entite &pet, Time dt, RenderWindow &app)
{
    //------ Déplacement PET------//
    float deltaPlayerX = player.posX + 10- pet.posX; //distance entre player et mob en x
    float deltaPlayerY = player.posY + 50  - pet.posY; //distance entre player et mob en y

    float hyp = sqrt((deltaPlayerX * deltaPlayerX) + ((deltaPlayerY ) * (deltaPlayerY)));

    pet.speedX = (hyp / pet.speed) * deltaPlayerX ;
    pet.speedY = (hyp / pet.speed) * deltaPlayerY ;

    if(hyp < 20)
    {
        pet.speedX = 0;
        pet.speedY = 0;
    }
    //printf("%i", pet.pointsdevie);
    if (pet.speedX > 0)
        pet.sprite.setTexture(pet.texture[(pet.vieMax - pet.pointsdevie)*3 +1]);
    else if (pet.speedX < 0)
        pet.sprite.setTexture(pet.texture[(pet.vieMax - pet.pointsdevie)*3 + 2]);

    else
        pet.sprite.setTexture(pet.texture[(pet.vieMax - pet.pointsdevie)*3]);

    pet.posX += pet.speedX * dt.asSeconds();
    pet.posY += pet.speedY * dt.asSeconds();
    pet.sprite.setPosition(pet.posX, pet.posY);

}
