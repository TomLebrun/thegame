#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "fonction.hpp"


int main()
{
    srand(time(NULL));
    // Create the main window
    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Mortal Resurection");
    Image icon;

    if(!icon.loadFromFile("Ressources/icon/icon.png"))
        printf("bug load icon");

    app.setIcon(64,64,icon.getPixelsPtr());
    app.setFramerateLimit(60);

    //Chargement de la musique
    Music musique1;
    if (!musique1.openFromFile("Ressources/musique/Dungeon-Cave_loop.ogg"))
        printf(("Erreur chargement de la musique"));
    musique1.setPosition(0, 1, 10);
    musique1.setPitch(1);
    musique1.setVolume(1);
    musique1.setLoop(true);

    Texture gameOverTexture;
    if(!gameOverTexture.loadFromFile("Ressources/map/GameOver.png"))
        printf("erreur charge gameover");
    Sprite gameoverSprite;
    gameoverSprite.setTexture(gameOverTexture);

    Texture victoireTexture;
    if(!victoireTexture.loadFromFile("Ressources/map/EcranVictoire.png"))
        printf("Erreur charge victoire");
    Sprite victoireSprite;
    victoireSprite.setTexture(victoireTexture);

    //chargement d'une la police d'ecriture
    Font MyFont;
    if (!MyFont.loadFromFile(FONT))
    {
        printf("erreur lors du chargement de la police");
    }

    Event event;

    musique1.play();

    int finBoucle = 1;

    int play = 0;
    // boucle tant que la fenetre est ouverte
    while (app.isOpen())
    {
        switch(play)
        {
        case 0 :
            // Lancement du menu
            play = menu(app, MyFont);
            break;

        case 1:
            // Lancement du jeu
            play = jeu(app, MyFont);
            break;

        case 2 :
            synopsis(app, MyFont);
            play = 0;
            break;


        case 3 :
            regles(app, MyFont);
            play = 0;
            break;

        case 4 :
            //ecran game over
            app.clear();
            app.draw(gameoverSprite);
            app.display();

            while (finBoucle == 1)
            {
                while(app.pollEvent(event))
                {
                    switch (event.type)
                    {
                    // fen�tre ferm�e
                    case Event::Closed:
                        app.close();
                        finBoucle = 0;
                        break;

                    // touche press�e
                    case Event::KeyPressed:
                        if (event.key.code == Keyboard::Enter)
                            finBoucle = 0;
                        break;


                    // we don't process other types of events
                    default:
                        break;
                    }
                }
            }
            play = 0;
            break;

        case 5:
            //ecran de fin de jeu
            app.clear();
            app.draw(victoireSprite);
            app.display();
            finBoucle = 1;
            while (finBoucle == 1)
            {
                while(app.pollEvent(event))
                {
                    switch (event.type)
                    {
                    // fen�tre ferm�e
                    case Event::Closed:
                        app.close();
                        finBoucle = 0;
                        break;

                    // touche press�e
                    case Event::KeyPressed:
                        if (event.key.code == Keyboard::Enter)
                            finBoucle = 0;
                        break;


                    // we don't process other types of events
                    default:
                        break;
                    }
                }
            }
            play = 0;
            break;

        case 7:
            app.close();
            break;
        }


    }

    return 0;
}
